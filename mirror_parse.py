import sqlite3

# Set up sqlite database in memory
mirror_db = sqlite3.connect(":memory:")
cursor = mirror_db.cursor()

# Create mirror table
architectures = ["amd64", "armel", "armhf", "hurd-i386", "i386", "ia64", "kfreebsd-amd64", "kfreebsd-i386", "mips", "mipsel", "powerpc", "s390", "s390x", "sparc"]
cursor.execute("CREATE TABLE mirrors(address TEXT PRIMARY KEY NOT NULL, " + ''.join(a.replace("-", "") + " INTEGER, " for a in architectures) +
               "countrycode TEXT, countryname TEXT, ftpdir TEXT, httpdir TEXT)")

# Parse mirror file
country_code = ""
country_name = ""
address = ""
mirror_architectures = ""
in_header = True
new_country_block = True
ftp_char_index = 0
http_char_index = 0
mirror_has_ftp = False
mirror_has_http = False
all_countries = []
with open("README.mirrors.txt", "r") as mirror_file:
    for line in mirror_file.readlines():
        if in_header:
            # Skip the header
            if line.startswith("HOST NAME"):
                in_header = False

                # Get position of FTP and HTTP title,
                # so we can distinguish column entries later
                ftp_char_index = line.index("FTP")
                http_char_index = line.index("HTTP")
                arch_char_index = line.index("ARCHITECTURES")
        else:
            # Header is done now we read in mirrors
            if line.startswith("---") or line.startswith("Last modified"):
                # Ignore underlinings or last line of file
                continue
            else:
                if not line.strip():
                    # We reached end of Country Block
                    new_country_block = True
                else:
                    if new_country_block:
                        # Last country block has ended, now read in new country name
                        line = line.split()
                        country_code = line[0]
                        country_name = ''.join(line[1:])
                        new_country_block = False

                        # Gather Countries
                        if country_code not in all_countries:
                            all_countries.append(country_code)
                    else:
                        # Now this must be a mirror
                        ftp_path = ""
                        http_path = ""
                        if line.strip().startswith("("):
                            # Ignore Mirror alias
                            continue
                        else:
                            # Check if mirror has FTP and/or HTTP
                            if line[ftp_char_index] != ' ':
                                mirror_has_ftp = True
                            else:
                                mirror_has_ftp = False

                            if line[http_char_index] != ' ':
                                mirror_has_http = True
                            else:
                                mirror_has_http = False

                            address = line[0:ftp_char_index].strip()
                            if mirror_has_ftp:
                                ftp_path = line[ftp_char_index:http_char_index].strip()
                            if mirror_has_http:
                                http_path = line[http_char_index:arch_char_index].strip()
                            if not mirror_has_ftp and not mirror_has_http:
                                # Something is wrong, mirror does not have FTP nor HTTP
                                print "Parsing Mirrorfile error: Could not parse mirror (Mirror appears to have neither http nor ftp)"
                                exit(1)

                            # Insert mirror into database
                            mirror_architectures = line[arch_char_index:]
                            arch_insert_str = ""
                            for arch in architectures:
                                if arch in mirror_architectures:
                                    arch_insert_str += "1, "
                                else:
                                    arch_insert_str += "0, "
                            cursor.execute("INSERT INTO mirrors(" + ''.join(a.replace("-", "") + ", " for a in architectures) + "address, countrycode, countryname, ftpdir, httpdir)"
                                           "VALUES (" + arch_insert_str + "\"" + address + "\", \"" + country_code + "\", \"" + country_name + "\", \"" + ftp_path + "\", \"" + http_path + "\"" + ")")

    mirror_db.commit()
